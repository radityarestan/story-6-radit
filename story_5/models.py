from django.db import models

pilihan_semester = ( 
    ("2020/2021-Gasal", "2020/2021-Gasal"),
    ("2020/2021-Genap", "2020/2021-Genap"), 
)

pilihan_sks = zip(range(1,7), range(1,7))


# Create your models here.
class MataKuliah(models.Model):
    matkul = models.CharField('Matakuliah', max_length=100, null=True)
    sks = models.IntegerField('sks', choices=pilihan_sks, default= '3', null=True)
    deskripsi = models.TextField('Deskripsi', null=True)
    dosen = models.CharField('Dosen', max_length=100, null=True)
    ruang = models.CharField('Ruang', max_length=100, null=True)
    semester = models.CharField('Semester', null=True, max_length=20, choices = pilihan_semester, default = '2020/2021-Gasal')
