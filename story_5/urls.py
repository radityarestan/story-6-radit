from django.urls import path

from . import views

app_name = 'story_5'

urlpatterns = [
    path('', views.index, name='index'),
    path('isi_matkul/', views.isiMatkul, name = 'isiMatkul'),
    path('isi_matkul/tambah_matkul/', views.tambahMatkul, name = 'tambahMakul'),
    path('detail_matkul/<int:matkul_id>', views.detailMatkul, name = 'detailMatkul'),
    path('hapus_matkul/<int:matkul_id>', views.hapusMatkul, name = 'hapusMatkul'),
]