from django.forms import ModelForm
from .models import MataKuliah


class FormulirMatkul(ModelForm):
    required_css_class = 'required'

    class Meta:
        model = MataKuliah
        fields = '__all__'
