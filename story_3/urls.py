from django.urls import path

from . import views

app_name = 'story_3'

urlpatterns = [
    path('', views.index, name='index'),
    path('about_me/', views.about_me, name = 'about_me'),
    path('activities/', views.activities, name = 'activities'),
    path('my_project/', views.my_project, name = 'my_project'),
]