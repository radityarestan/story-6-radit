from django.shortcuts import render

# Create your views here.

def index(request) :
    return render(request, 'story-3.html')

def about_me(request) :
    return render(request, 'about_me.html')

def activities(request) :
    return render(request, 'activities.html')

def my_project(request) :
    return render(request, 'my_project.html')
