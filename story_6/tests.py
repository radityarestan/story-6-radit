from django.test import TestCase, Client
from django.urls import resolve, reverse
from .models import Kegiatan, Partisipan
from . import views

# Create your tests here.
class Story6Home(TestCase):
    def test_apakah_url_home_ada(self):
        response = Client().get('/story6/')
        self.assertEqual(response.status_code, 200)

    def test_apakah_terdapat_fungsi_index(self):
        found = resolve('/story6/')
        self.assertEqual(found.func, views.index)

    def test_apakah_template_home_sudah_sesuai(self):
        response = Client().get('/story6/')
        self.assertTemplateUsed(response, 'story-6.html')

    def test_apakah_judul_dan_tombol_ada(self):
        response = Client().get('/story6/')
        html_response = response.content.decode('utf8')
        self.assertIn("Daftar", html_response)
        self.assertIn("Kegiatan", html_response)
        self.assertIn("Tambah Kegiatan", html_response)

    def test_apakah_objek_kegiatan_ada(self):
        Kegiatan.objects.create(nama = "Belajar Bareng", deskripsi = "Ya belajar bersama")
        self.assertEqual(Kegiatan.objects.all().count(), 1)


class Story6IsiKegiatan(TestCase):
    def test_apakah_url_isi_kegiatan_ada(self):
        response = Client().get('/story6/isi_kegiatan/')
        self.assertEqual(response.status_code, 200)

    def test_apakah_terdapat_fungsi_isi_kegiatan(self):
        found = resolve('/story6/isi_kegiatan/')
        self.assertEqual(found.func, views.isiKegiatan)

    def test_apakah_template_isi_kegiatan_ada(self):
        response = Client().get('/story6/isi_kegiatan/')
        self.assertTemplateUsed(response, 'isi-kegiatan.html')

    def test_form_dan_submit_kegiatan_ada(self):
        response = Client().get('/story6/isi_kegiatan/')
        html_response = response.content.decode('utf8')
        self.assertIn("Nama", html_response)
        self.assertIn("Deskripsi", html_response)
        self.assertIn("Submit", html_response)


class Story6DaftarKegiatan(TestCase):
    def test_apakah_url_daftar_kegiatan_ada(self):
        response = Client().get('/story6/daftar_kegiatan/')
        self.assertEqual(response.status_code, 200)

    def test_apakah_terdapat_fungsi_daftar_kegiatan(self):
        found = resolve('/story6/daftar_kegiatan/')
        self.assertEqual(found.func, views.daftarKegiatan)

    def test_apakah_template_daftar_kegiatan_ada(self):
        response = Client().get('/story6/daftar_kegiatan/')
        self.assertTemplateUsed(response, 'isi-partisipan.html')

    def test_form_dan_submit_orang_ada(self):
        response = Client().get('/story6/daftar_kegiatan/')
        html_response = response.content.decode('utf8')
        self.assertIn("Nama", html_response)
        self.assertIn("Asal Daerah", html_response)
        self.assertIn("Kegiatan", html_response)
        self.assertIn("Submit", html_response)