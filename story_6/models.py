from django.db import models

# Create your models here.
class Kegiatan(models.Model):
    nama = models.CharField('Nama', max_length = 50, null=True)
    deskripsi = models.TextField('Deskripsi', null = True)

    def __str__(self):
        return self.nama

class Partisipan(models.Model):
    nama = models.CharField('Nama', max_length = 50, null=True)
    asal = models.CharField('Asal Daerah', max_length = 50, null=True)
    kegiatan = models.ManyToManyField(Kegiatan)