from django.forms import ModelForm
from .models import Kegiatan, Partisipan


class FormulirKegiatan(ModelForm):
    required_css_class = 'required'

    class Meta:
        model = Kegiatan
        fields = '__all__'

class FormulirPartisipan(ModelForm):
    required_css_class = 'required'

    class Meta:
        model = Partisipan
        fields = '__all__'
