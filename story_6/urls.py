from django.urls import path

from . import views

app_name = 'story_6'

urlpatterns = [
    path('', views.index, name='index'),
    path('daftar_kegiatan/', views.daftarKegiatan, name = 'daftarKegiatan'),
    path('daftar_kegiatan/tambah_partisipan/', views.tambahPartisipan, name = 'tambahPartisipan'),
    path('isi_kegiatan/', views.isiKegiatan, name = 'isiKegiatan'),
    path('isi_kegiatan/tambah_kegiatan/', views.tambahKegiatan, name = 'tambahKegiatan'),
    path('detail_partisipan/<int:kegiatan_id>', views.detailPartisipan, name = 'detailKegiatan'),
    path('detail_partisipan/hapus_orang/<int:orang_id>/<int:kegiatan_id>', views.hapusOrang, name = 'hapus_orang'),
    path('hapus_kegiatan/<int:kegiatan_id>', views.hapusKegiatan, name = 'hapusKegiatan'),
]