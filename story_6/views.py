from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Kegiatan, Partisipan
from .forms import FormulirKegiatan, FormulirPartisipan

# Create your views here.
def index(request):
    kumpulan_kegiatan = Kegiatan.objects.all()
    return render(request, 'story-6.html', {'kumpulan_kegiatan' : kumpulan_kegiatan})

def isiKegiatan(request):
    form = FormulirKegiatan()
    return render(request, 'isi-kegiatan.html', {'form' : form})

def tambahKegiatan(request):
    if request.method == 'POST' :
        form = FormulirKegiatan(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/story6/')

def tambahPartisipan(request):
    if request.method == 'POST' :
        form = FormulirPartisipan(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/story6/')


def detailPartisipan(request, kegiatan_id):
    detail_kegiatan = Kegiatan.objects.get(id=kegiatan_id)
    daftar_orang = Partisipan.objects.filter(kegiatan__id=kegiatan_id)
    return render(request, 'detail-kegiatan.html', {"detail_kegiatan" : detail_kegiatan, "daftar_orang" : daftar_orang})

def daftarKegiatan(request):
    form = FormulirPartisipan()
    return render(request, 'isi-partisipan.html', {'form' : form})

def hapusKegiatan(request, kegiatan_id):
    kegiatan_dihapus = Kegiatan.objects.get(id=kegiatan_id)
    kegiatan_dihapus.delete()
    return HttpResponseRedirect('/story6/')

def hapusOrang(request, orang_id, kegiatan_id):
    orang_dihapus = Partisipan.objects.get(id = orang_id)
    kegiatan_yang_dicancel = Kegiatan.objects.get(id = kegiatan_id)
    orang_dihapus.kegiatan.remove(kegiatan_yang_dicancel)
    return HttpResponseRedirect('/story6/')